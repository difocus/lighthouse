# Changelog

## v2.1.0 (2023-07-07)

### Changed

- Updated base image to Debian Bookworm. (#7)

## v2.0.6 (2023-06-15)

### Fixed

- Updated to `lighthouse@10.3.0`.

## v2.0.5 (2023-06-03)

### Fixed

- Updated to Chromium 114.

### Miscellaneous

- Updated pipeline `needs` to optimize execution.

## v2.0.4 (2023-05-04)

### Fixed

- Updated to `lighthouse@10.2.0`.
- Update to Chromium 113.

### Miscellaneous

- Updated pipeline `needs` to optimize execution. (#6)

## v2.0.3 (2023-04-26)

### Fixed

- Added `procps` package to image to resolve issues spawning child processes.
  (#5)

## v2.0.2 (2023-04-14)

### Fixed

- Updated to `lighthouse@10.1.1` and `@lhci/cli@0.12.0`.

## v2.0.1 (2023-04-12)

### Fixed

- Updated to `lighthouse@10.1.0` and `@lhci/cli@0.11.1`.
- Update to Chromium 112.

## v2.0.0 (2023-02-21)

### Changed

- BREAKING: Updated to `lighthouse@10.0.1`. See the
  [breaking changes](https://github.com/GoogleChrome/lighthouse/releases/tag/v10.0.0)
  in v10.0.0.
- BREAKING: Updated to `@lhci/cli@0.11.0`. See the
  [breaking changes](https://github.com/GoogleChrome/lighthouse-ci/releases/tag/v0.11.0)
  in v0.11.0. Note this still uses `lighthouse@9.6.8`.

## v1.2.0 (2023-02-12)

### Changed

- Update project to pin all dependency versions (including transitive npm
  dependencies). Update renovate config to manage all dependency updates. (#1)

### Miscellaneous

- Added `prettier` pre-commit hook and CI job to check formatting.

## v1.1.1 (2023-02-11)

### Fixed

- Pinned to `lighthouse@9.6.8` until v10 issues can be resolved. (#4)

## v1.1.0 (2023-01-16)

### Changed

- Added [Lighthouse CI](https://www.npmjs.com/package/@lhci/cli) to the
  image, which can be run instead of Lighthouse when required. The other
  dependencies all remain the same.

## v1.0.0 (2023-01-15)

Initial release
